export default defineNuxtConfig({
	modules: [
		'@nuxt/content',
		'@nuxtjs/tailwindcss',
		'@nuxt/image-edge'
	],

	content: {
		documentDriven: true
	},

	tailwindcss: {
		cssPath: '~/assets/css/main.css',
	},

	app: {
		head: {
			charset: 'utf-16',
			viewport: 'width=500, initial-scale=1',
			link: [
				{ 'rel': "apple-touch-icon", 'sizes': "180x180", 'href': "/apple-touch-icon.png" },
				{ 'rel': "icon", 'type': "image/png", 'sizes': "32x32", 'href': "/favicon-32x32.png" },
				{ 'rel': "icon", 'type': "image/png", 'sizes': "16x16", 'href': "/favicon-16x16.png" },
				{ 'rel': "manifest", 'href': "/site.webmanifest" },
				{ 'rel': "mask-icon", 'href': "/safari-pinned-tab.svg", 'color': "#5bbad5" }
			],
			meta: [
				{ 'name': "msapplication-TileColor", 'content': "#da532c" },
				{ 'name': "theme-color", 'content': "#ffffff" },
				{ 'name': "copyright", 'content': "© marcguinea.com" },
				{ 'name': "author", 'content': "Marc Guinea" }
			]
		}
	},
})
