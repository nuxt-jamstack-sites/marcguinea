---
layout: resume
---
# Marc Guinea

Software Developer with over 9 years of experience specialized in PHP for small to large companies. Focused on best practices, deliver quality code and manage tech teams.

## Work experience

### Ankorstore

**Senior backend engineer** *from 2022 to now*

Develop new features and mantain application in shipping context.

<span class="badge">PHP</span>
<span class="badge">GCP</span>
<span class="badge">Grafana</span>
<span class="badge">PHPUnit</span>
<span class="badge">Circle CI</span>
<span class="badge">Github Actions</span>
<span class="badge">Docker</span>
<span class="badge">Laravel</span>

### Zinio

**Senior backend engineer** *from 2021 to 2022 (1 year)*

Maintainment of microservices, implementation of new ones based on PHP.

<span class="badge">PHP</span>
<span class="badge">AWS</span>
<span class="badge">PHPUnit</span>
<span class="badge">Circle CI</span>
<span class="badge">Docker</span>
<span class="badge">Symfony</span>
<span class="badge">Python</span>
<span class="badge">Serverless</span>
<span class="badge">Event Sourcing</span>
<span class="badge">Microservices</span>

### Deliverea

**Senior PHP backend developer** *from 2020 to 2021 (1 year)*

Develop and analysis of new features over Symfony framework 4.4 applying best practices in an hexagonal DDD architecture through CQRS approach in an API application. Vue.js minor developments for frontend.

<span class="badge">PHP</span>
<span class="badge">AWS</span>
<span class="badge">Codeception</span>
<span class="badge">Docker</span>
<span class="badge">Symfony</span>
<span class="badge">Vue</span>
<span class="badge">Microservices</span>

### Intercom

**Senior PHP backend developer** *from 2020 to 2020 (1 year)*

Develop backend features and fixes over Symfony framework 3.2 and custom framework.

<span class="badge">PHP</span>
<span class="badge">Jenkins</span>
<span class="badge">Symfony</span>
<span class="badge">Docker</span>

### La Teva Web

**Tech lead** *from 2018 to 2020 (2 years)*

Tech lead and Laravel architect developing projects and custom CMS using distributed code over composer packages. Vue.js frontend development and DevOps.

<span class="badge">PHP</span>
<span class="badge">Gitlab</span>
<span class="badge">Laravel</span>
<span class="badge">Docker</span>
<span class="badge">Vue</span>
<span class="badge">Nuxt</span>
<span class="badge">Ansible</span>
<span class="badge">Terraform</span>
<span class="badge">Packagist</span>

### Ricoh

**Senior PHP developer**  *from 2015 to 2018 (3 years)*

Analyst developer for documentation projects based in PHP using Laravel and BPM. Integrations with third party Saas and customer integrations.

<span class="badge">PHP</span>
<span class="badge">BPM</span>
<span class="badge">Laravel</span>

### WinWorld

**Junior fullstack developer**  *from 2014 to 2015 (1 year)*

Develop and analysis of new features over existing projects based on Laravel.

<span class="badge">PHP</span>
<span class="badge">Javascript</span>
<span class="badge">Python</span>
<span class="badge">.NET</span>
<span class="badge">Laravel</span>

### BBVA

**Analyst and Seller** *from 2006 to 2013 (7 years)*

Insurances and active - passive bank products analysis and selling.

<span class="badge">Finance</span>

## Skills

**Technical** 

- Languages: PHP, Javascript, Go.
- Patterns: SOLID, CQRS, DDD, TDD, BDD, Hexagonal, Design patterns.
- Frameworks: Laravel, Symfony, Slim, Vue, Nuxt, Codeception, PHPUnit, Tailwind CSS.
- DevOps: Terraform, GitLab, GitHub, CircleCI, Docker, Kubernetes, AWS, GCP.
- Databases: MySQL, PostgreSQL, MariaDB, MongoDB, Redis.
- Other: RabbitMQ, Grafana

**Another skills** 

- Kaban
- Scrum
- Team player
- Mentoring

**Languages**

- English
- Spanish
- Catalan

## Education

### Master in videogames design and development

**UPC** *from 2015 to 2016*

Development of videogames using c++ and Unity.

### CFGS DAI

**Carles Vallbona** *from 2004 to 2006*

Software development applications degree.

---

**site**: [https://www.marcguinea.com](https://www.marcguinea.com) **linkedin**: [linkedin.com/in/marcguinea](linkedin.com/in/marcguinea)
